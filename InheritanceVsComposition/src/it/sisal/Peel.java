package it.sisal;

public class Peel {
	private int peelCount;

	public int getPeelCount() {
		return peelCount;
	}

	public void setPeelCount(int peelCount) {
		this.peelCount = peelCount;
	}

	public Peel(int peelCount) {
		super();
		this.peelCount = peelCount;
	}
	
	
}
