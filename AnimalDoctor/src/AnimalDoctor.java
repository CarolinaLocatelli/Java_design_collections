import java.util.List;

import prova.Animal;
import prova.Dog;

public class AnimalDoctor {
	/*public void checkAnimals(Animal[] a){
		for (Animal a1 : a){
			a1.check();
		}
	}
	
	public void addDog(Animal[] a){
		a[0] = new Dog();
	}*/
		
		public void checkAnimals(List<? extends Animal> animals){
			for(int i=0; i<animals.size(); i++){
				((Animal)animals.get(i)).check();
			}
		}
		
		public void addDog(List<? super Dog> animals){
			animals.add(new Dog());
		}
}
