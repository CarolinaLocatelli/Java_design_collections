import java.util.ArrayList;
import java.util.List;

import prova.Animal;
import prova.Bird;
import prova.Cat;
import prova.Dog;

public class Main {
	public static void main(String[] args){
		/*Dog[] dogs = {new Dog(), new Dog()};
		Cat[] cats = {new Cat()};
		Bird[] birds = {new Bird(), new Bird(), new Bird()};*/
		List<Dog> dogs = new ArrayList<>();
		List<Cat> cats = new ArrayList<>();
		List<Bird> birds = new ArrayList<>();
		
		dogs.add(new Dog());
		cats.add(new Cat());
		birds.add(new Bird());
		
		AnimalDoctor doc = new AnimalDoctor();
		doc.checkAnimals(dogs);
		doc.checkAnimals(cats);
		doc.checkAnimals(birds);
		
		doc.addDog(dogs);
		//doc.addDog(cats);
	}
		
}
