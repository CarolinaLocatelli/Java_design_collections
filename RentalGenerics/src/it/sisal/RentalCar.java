package it.sisal;

import java.util.List;

public class RentalCar extends Rental{
	public RentalCar(List<Car> rentalPool, int max) {
		super(rentalPool, max);
		// TODO Auto-generated constructor stub
	}

	private List<Car> rentalPool;
	private int max;
	
	//apply polymorphism
	@Override
	public Car getRental(){
		return (Car)super.getRental();
	}
	
	@Override
	public void returnRental(Object o){
		if(o instanceof Car){
			rentalPool.add((Car) o);
		} else {
			System.out.println("tried to insert a non-car object");
		}
	}
}
