package it.sisal;

import java.util.List;

public class RentalGeneric<T> {
	private List<T> rentalPool;
	private int max;
	
	public RentalGeneric(List<T> rentalPool, int max) {
		super();
		this.rentalPool = rentalPool;
		this.max = max;
	}
	
	public T getRental(){
		return rentalPool.get(0);
	}
	
	public void returnRental(T t){
		rentalPool.add(t);
	}
}
