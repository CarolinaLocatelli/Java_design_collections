package it.sisal;

import java.util.List;

public class Rental {
	List rentalPool;
	int max;
	public Rental(List rentalPool, int max) {
		super();
		this.rentalPool = rentalPool;
		this.max = max;
	}
	
	public Object getRental(){
		return (Object)rentalPool.get(0);
	}
	
	public void returnRental(Object o){
		rentalPool.add(o);
	}
}
