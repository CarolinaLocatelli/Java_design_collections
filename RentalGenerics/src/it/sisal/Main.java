package it.sisal;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args){
		List<Car> cars = new ArrayList<>();
		cars.add(new Car());
		
		RentalGeneric<Car> rg = new RentalGeneric<>(cars, 1);
		
		Car rentedCar = rg.getRental();
		
		rg.returnRental(rentedCar);
	}
}
