package prova;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args){
		List<Integer> myInts = new ArrayList<>();
		myInts.add(32);
		myInts.add(10);
		myInts.add(10);
		
		Adder adder = new Adder();
		
		System.out.println(adder.computeTot(myInts));
		
		Inserter inserter = new Inserter();
		inserter.insertElement(myInts);
		
		for(int i = 0; i<myInts.size(); i++){
			System.out.println(myInts.get(i));
		}
	}
}
