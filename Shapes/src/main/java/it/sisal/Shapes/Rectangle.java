package it.sisal.Shapes;

public class Rectangle extends Shape{
	private Point origin;
	private int width;
	private int height;
	public Point getOrigin() {
		return origin;
	}
	public void setOrigin(Point origin) {
		this.origin = origin;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	@Override
	public double calcArea(){
		return height*width;
	}
}
