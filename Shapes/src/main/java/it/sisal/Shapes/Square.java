package it.sisal.Shapes;

public class Square extends Shape{
	private Point origin;
	private int side;
	public Point getOrigin() {
		return origin;
	}
	public void setOrigin(Point origin) {
		this.origin = origin;
	}
	public int getSide() {
		return side;
	}
	public void setSide(int side) {
		this.side = side;
	}
	
	@Override
	public double calcArea(){
		return side*side;
	}
}
