package it.sisal.Shapes;

public class Circle extends Shape {
	private Point origin;
	private int radius;
	public Point getOrigin() {
		return origin;
	}
	public void setOrigin(Point origin) {
		this.origin = origin;
	}
	public int getRadius() {
		return radius;
	}
	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	@Override
	public double calcArea(){
		return radius*radius*Math.PI;
	}
}
