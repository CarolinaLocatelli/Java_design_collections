package it.sisal.Shapes;

import java.util.ArrayList;
import java.util.List;

public class Canvas {
	private List shapes = new ArrayList();
	
	/*public void addCircle(Circle circle){
		shapes.add(circle);
	}
	
	public void addRectangle(Rectangle rect){
		shapes.add(rect); //DRY!!!!!!
	}
	
	public void addSquare(Square square){
		shapes.add(square);
	}*/
	
	public void addShape(Shape shape){
		shapes.add(shape);
	}
	
	/*public double calcTotArea(){
		double totArea = 0.0;
		for(int i = 0; i<shapes.size(); i++){
			if(shapes.get(i) instanceof Circle){
				totArea += ((Circle)shapes.get(i)).calcArea();
			}
			if(shapes.get(i) instanceof Rectangle){
				totArea += ((Rectangle)shapes.get(i)).calcArea();
			}
			if(shapes.get(i) instanceof Square){
				totArea += ((Square)shapes.get(i)).calcArea();
			}
		}
		return totArea;
	}*/
	
	public double calcTotArea(){
		double totArea = 0.0;
		for(int i = 0; i<shapes.size(); i++){
			totArea += ((Shape)shapes.get(i)).calcArea();
		}
		return totArea;
	}
}
