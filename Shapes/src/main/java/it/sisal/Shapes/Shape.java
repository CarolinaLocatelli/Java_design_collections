package it.sisal.Shapes;

public abstract class Shape {
	private Point origin;
	
	public abstract double calcArea();
	
	
}
