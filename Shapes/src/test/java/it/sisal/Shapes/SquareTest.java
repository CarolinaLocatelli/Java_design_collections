package it.sisal.Shapes;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class SquareTest {

	@Test
	public void test() {
		Point origin = new Point();
		origin.setX(0);
		origin.setY(0);
		
		Square square = new Square();
		square.setOrigin(origin);
		square.setSide(2);
		
		Assert.assertEquals(2.0*2.0, square.calcArea());
	}

}
