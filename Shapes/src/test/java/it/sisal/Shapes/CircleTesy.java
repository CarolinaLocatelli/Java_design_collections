package it.sisal.Shapes;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class CircleTesy {

	@Test
	public void test() {
		
		Point origin = new Point();
		origin.setX(0);
		origin.setY(0);
		
		Circle circle = new Circle();
		circle.setOrigin(origin);
		circle.setRadius(2);
		
		Assert.assertEquals(2*2*Math.PI, circle.calcArea());
		
		
	}

}
