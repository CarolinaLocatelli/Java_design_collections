package it.sisal.Shapes;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class CanvasTest {

	@Test
	public void test() {
		Point origin = new Point();
		origin.setX(0);
		origin.setY(0);
		
		Circle circle = new Circle();
		circle.setOrigin(origin);
		circle.setRadius(2);
		
		Rectangle rect = new Rectangle();
		rect.setHeight(2);
		rect.setOrigin(origin);
		rect.setWidth(3);
		
		Square square = new Square();
		square.setOrigin(origin);
		square.setSide(2);
		
		Canvas canvas = new Canvas();
		/*canvas.addCircle(circle);
		canvas.addRectangle(rect);
		canvas.addSquare(square);*/
		
		canvas.addShape(square);
		canvas.addShape(rect);
		canvas.addShape(circle);
		
		
		Assert.assertEquals(2*2*Math.PI + 6.0 + 4.0, canvas.calcTotArea());
		
	}

}
