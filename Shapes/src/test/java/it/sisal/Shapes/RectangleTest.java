package it.sisal.Shapes;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class RectangleTest {

	@Test
	public void test() {
		Point origin = new Point();
		origin.setX(0);
		origin.setY(0);
		
		Rectangle rect = new Rectangle();
		rect.setHeight(2);
		rect.setOrigin(origin);
		rect.setWidth(3);
		
		ExtendedSquare es = new ExtendedSquare();
		es.setHeight(2);
		es.setWidth(3);
		
		Assert.assertEquals(2.0*3.0, es.calcArea());
	}

}
