package prova;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Sort {
	public static void main(String[] args){
		List<String> strings = new ArrayList<>();
		List<Fruit>	fruits = new ArrayList<>();
		
		strings.add("strawberry");
		strings.add("apple");
		strings.add("pear");
		
		System.out.println("Not sorted elements:");
		for(String s : strings){
			System.out.println(s);
		}
		
		Collections.sort(strings);
		System.out.println("Sorted elements:");
		for(String s : strings){
			System.out.println(s);
		}
		
		
		Fruit strawberry = new Fruit("strawberry", 15);
		Fruit apple = new Fruit("apple", 3);
		Fruit pear = new Fruit("pear", 45);
		
		fruits.add(strawberry);
		fruits.add(apple);
		fruits.add(pear);
		
		System.out.println("Not sorted elements:");
		for(Fruit f : fruits){
			System.out.println(f.getName());
		}
		
		Collections.sort(fruits);
		System.out.println("Sorted elements by quantity:");
		for(Fruit f : fruits){
			System.out.println(f.getName()+" - " +f.getQuantity());
		}
		
		Collections.sort(fruits, new FruitNameComparator());
		System.out.println("Sorted elements by name:");
		for(Fruit f : fruits){
			System.out.println(f.getName()+" - " +f.getQuantity());
		}
		
		
		System.out.println("Binary search");
		int idx=Collections.binarySearch(strings, "apple");
		System.out.println("The index of apple in the name-sorted arraylist of strings is "+ idx);
		
		
		int idx1=Collections.binarySearch(fruits, apple, new FruitNameComparator());
		System.out.println("The index of apple in the name-sorted arraylist of fruits is "+ idx1);
		
	}
}
