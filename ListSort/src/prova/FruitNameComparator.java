package prova;

import java.util.Comparator;

public class FruitNameComparator implements Comparator<Fruit>{

	@Override
	public int compare(Fruit arg0, Fruit arg1) {
		return arg0.getName().compareTo(arg1.getName());
	}

}
