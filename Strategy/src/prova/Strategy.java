package prova;

public interface Strategy {
	public int doOperation(int i1, int i2);
}
