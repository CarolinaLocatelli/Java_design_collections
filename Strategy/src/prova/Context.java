package prova;

public class Context {
	private Strategy strategy;

	public Context(Strategy strategy) {
		super();
		this.strategy = strategy;
	}
	
	public int executeStrategy(int i1, int i2){
		return strategy.doOperation(i1, i2);
	}
}
