package prova;

public class Main {
	public static void main(String[] args){
		Game game = new Cricket();
		
		game.initializeGame();
		game.startGame();
		game.endGame();
		
		game = new Football();
		game.initializeGame();
		game.startGame();
	}
}
