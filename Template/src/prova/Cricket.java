package prova;

public class Cricket extends Game{

	@Override
	public void initializeGame() {
		System.out.println("Initialized Cricket");
		
	}

	@Override
	public void startGame() {
		System.out.println("Started playing Cricket");
		
	}

	@Override
	public void endGame() {
		System.out.println("End playing Cricket");
		
	}

}
